// Craps.java

package co.vu.kaiyin.craps;

import java.security.SecureRandom;

public class Craps {
    public static enum Outcome {ONGOING, WIN_FIRST, WIN_POINT, LOSE_FIRST, LOSE_POINT};
    private int currentNumber;
    private final int point;
    private Outcome gameResult = Outcome.ONGOING;
    private static SecureRandom randomNumbers = new SecureRandom();

    public static int rollDice() {
        return 2 + randomNumbers.nextInt(6) + randomNumbers.nextInt(6);
    }

    public Craps() {
        currentNumber = rollDice();
        switch (currentNumber) {
            case 7:case  11:
                gameResult = Outcome.WIN_FIRST;
                point = 0;
                break;
            case 2:case 3:case 12:
                gameResult = Outcome.LOSE_FIRST;
                point = 0;
                break;
            default:
                point = currentNumber;
        }
    }

    public void moreRolls() {
        currentNumber = rollDice();
        if(currentNumber == point) {
            gameResult = Outcome.WIN_POINT;
        } else if(currentNumber == 7) {
            gameResult = Outcome.LOSE_POINT;
        }
    }


    public Outcome getGameResult() {
        return gameResult;
    }

    public int getPoint() {
        return point;
    }

    public int getCurrentNumber() {
        return currentNumber;
    }
}


// UseCraps.java

package co.vu.kaiyin.craps;

import java.io.IOException;
import java.util.Scanner;

public class UseCraps {
    public static void main(String[] args) throws IOException {
        final Craps crapObj = new Craps();
        waitUser();
        declareResult(crapObj);
        while(crapObj.getGameResult() == Craps.Outcome.ONGOING) {
            waitUser();
            crapObj.moreRolls();
            declareResult(crapObj);
        }
    }
    public static void declareResult(Craps pCrap) {
        System.out.printf("You got a %d\n", pCrap.getCurrentNumber());
        int point = pCrap.getPoint();
        if(point != 0) {
            System.out.printf("You need to make your point: %d\n", point);
        }
        switch (pCrap.getGameResult()) {
            case LOSE_FIRST:
                System.out.println("You have lost because of hitting crap in the first round.");
                break;
            case WIN_FIRST:
                System.out.println("You have won, you got a lucky number in the first round!");
                break;
            case LOSE_POINT:
                System.out.println("You have lost, you hit 7 before making your point!");
                break;
            case WIN_POINT:
                System.out.println("You have won, you made your point!");
                break;
            case ONGOING:
                System.out.println("Game continues...\n");
                break;
        }
        System.out.println();
    }

    private static void waitUser() {
        Scanner inputScan = new Scanner(System.in);
        System.out.print("Press enter to start rolling: ");
        String input = inputScan.nextLine();
    }
}

